#!/usr/bin/env python3

import os
from pathlib import Path
import getpass

file_name = "shell.nix"
current_dir = os.getcwd()

while True:
    files = os.listdir(current_dir)
    parent_dir = os.path.dirname(current_dir)
    file_path = os.path.join(current_dir, file_name)
    if os.path.isfile(file_path):
        file_info = Path(file_path)
        if file_info.owner() == getpass.getuser():
            os.system(f"$EDITOR {file_path}")
        else:
            os.system(f"sudo $EDITOR {file_path}")
        break
    else:
        if current_dir == parent_dir:
            print("shell.nix not found in parent tree")
            exit(1)
        else:
            current_dir = parent_dir
