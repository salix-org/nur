{ python3Packages }:

with python3Packages;
buildPythonApplication {
  pname = "edit-parent-shell";
  version = "0.0.1";

  src = ./.;
}
