{ system ? builtins.currentSystem, pkgs ? import <nixpkgs> { inherit system; } }:

{
  edit-parent-shell = pkgs.callPackage ./pkgs/edit-parent-shell { };
}
